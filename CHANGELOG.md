## v0.1.52 (20/03/2020)
### Build
* fix failing build
* add dist check in build make command

## v0.1.51 (19/03/2020)
### Code
* fix code to pass pylint
* fix all block contains UD amount in block_forge.py
* fix old duniterpy output.conditions field remaining
* fix one aiohttp Deprecation Warning

## v0.1.50 (17/03/2020)
### Code
* upgrade duniterpy dependency to 0.56.0 and update code
* add dev tools: mypy, pylint and black
* cleanup code with mypy, pylint and black
* add deploy tools

